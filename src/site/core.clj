(ns site.core
  (:require [ring.util.response :refer :all]
            [compojure.core :refer :all]
            [clojure.pprint :refer :all]
            [compojure.route :as route]
            [hiccup.core :refer :all]
            [ring.middleware.params :refer [wrap-params]]
            [hiccup.form :refer :all]
            [hiccup.page :refer :all]))
(defn login [req]
  (html
   [:html
    [:head (include-css "style.css")
     [:meta {:charset "UTF-8"}
     [:title "Form"]]]
    [:body {:class "body1"}
     [:div {:class "vladmaxi-top"}
      [:a {:href "http://localhost:3000/"} "Главная страница"]
      [:span {:class "right"}]

     [:div {:class "clr"}]
     [:div {:id "login-form"}
      [:h1 "Авторизация на сайте"]
      [:fieldset
       [:form  {:action "secret" :method "POST"}
        [:input {:name "login" :type "text" :placeholder "Логин" } ]
        [:input {:name "password" :type "text"  :placeholder "Пароль" }]
        [:input {:type "submit" :class "button clearfix" :value "ВХОД"}]
        [:footer {:class "clearfix"}]]]]]]]))

(defn home []
  (html
    [:head (include-css "/style.css")	[:meta {:charset "UTF-8"}]
     [:title "Регистрация"]]
    [:body {:class "body2"}
     [:fieldset  [:h3 {:class "h2"} "Добро пожаловать!"]
      [:a {:class "button1" :href "http://localhost:3000/login"} "ВОЙТИ"]]]))
(defn secret []
  (html
    [:body {:class "body3"}
     [:head (include-css "/style.css")
      [:p "Вы успешно авторизованы!"]]]))
(defroutes webhandler
           (route/resources "/")
           (GET "/" [] (home))
           (GET "/login" [req] (login req))
           (POST "/secret" [login password :as req]
              (if (and (= login "admin")(= password "12345")) (secret) "false")))
(def app (wrap-params  webhandler))
